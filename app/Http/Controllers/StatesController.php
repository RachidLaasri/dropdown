<?php

namespace App\Http\Controllers;

use PragmaRX\Countries\Package\Countries;

class StatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($country)
    {
        $country = Countries::where('cca2', upper($country))
            ->first()
            ->hydrateStates();

        return $country->states->pluck('name', 'postal');
    }
}
