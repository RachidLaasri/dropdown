<?php

namespace App\Http\Controllers;

use PragmaRX\Countries\Package\Countries;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($country, $state)
    {
        $country = Countries::where('cca2', upper($country))
            ->first()
            ->hydrateCities()
            ->hydrateStates();

        $state = $country->states
            ->where('postal', $state)
            ->first()
            ->get('name');

        return $country->cities
            ->where('adm1name', $state)
            ->pluck('name', 'name');
    }
}
