<?php

namespace App\Http\Controllers;

use PragmaRX\Countries\Package\Countries;

class CountriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Countries::all()->pluck('name.common', 'cca2');
    }
}
