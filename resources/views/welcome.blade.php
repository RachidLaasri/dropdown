<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>Laravel countries dropdown</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <div id="app">
            <countries></countries>
        </div>
        <script src="/js/app.js"></script>
    </body>
</html>
